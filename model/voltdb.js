const voltdb = function () {
    const hdd = function () {
        return ram() * VOLTDB_INST + dlhdd();
    };
    const cpu = function () {
        return (input.consumerBaseMi.value < MinSUB)?
            4: 4 + (input.consumerBaseMi.value / mi * 0.6) + dlcpu();
    };
    const ram = function () {
        const cgConsumer = (65.53 * input.consumerBaseMi.value * input.controlGroup.value) / gbSize;
        const freqCount = input.precisionMarketerYN.value ? 86 * input.consumerBaseMi.value / gbSize : 0;
        const sendBc = input.precisionMarketerYN.value ? 1144 * input.noOfMessages.value / gbSize : 0;
        const eventsDouble = 95 * input.consumerBaseMi.value * input.noOfRealTimeKPI.value / gbSize;
        const eventsNo = eventsDouble;
        const eventsString = input.precisionMarketerYN.value ? (125 * input.noOfRealTimeKPI.value / gbSize) : 0;
        const dailyRewards = 1;
        const rewardBc = input.noOfMessages.value * 29 / gbSize;
        return cgConsumer + freqCount + sendBc + eventsDouble + eventsNo + eventsString + dailyRewards + rewardBc + dlram();
    };

    const dlcpu = () =>
         5 + (input.consumerBaseMi.value < fiveMi)?0: (input.consumerBaseMi.value / mi * 0.5);

    const dlram = () => 10 + (input.consumerBaseMi.value < fiveMi) ?0: (input.consumerBaseMi.value / mi * 1);

    const dlhdd =  () => dlram() * 3;

    return {hdd:hdd, cpu:cpu, ram:ram};
}();