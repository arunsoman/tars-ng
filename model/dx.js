dx = function () {
    let mods = [elastic, hadoop, mesos, mysql, spark, voltdb];
    let totalCPU = mods.reduce((a, b) => { a.total += b.cpu(); return a }, { total: 0 });
    let totalRAM = mods.reduce((a, b) => { a.total += b.ram(); return a }, { total: 0 });
    let totalHadoop = hadoop.hdd() + spark.hdd();
    let totalNfs = voltdb.hdd() + mysql.hdd() + mesos.hdd();
    let totalElastic = elastic.hdd();
    if (input.consumerBaseMi.value >= 21000000) {
    	totalCPU = Math.ceil(totalCPU.total * 1.5);
    	totalRAM = Math.ceil(totalRAM.total * 1.5);
    	totalHadoop = Math.ceil(totalHadoop * 2);
    }else {
    	totalCPU = Math.ceil(totalCPU.total);
    	totalRAM = Math.ceil(totalRAM.total);
    }

    let serverSpec = serverCalculator(totalCPU, totalRAM,
        totalHadoop, totalNfs, totalElastic);
    let networkSpec = networkManager(serverSpec.cnt, input.consumerBaseMi.value,
        input.feedSize.value);
    let switchIps = Array.apply(null, Array(serverSpec.cnt)).map(function (_, i) { return 'switchIp:' + i })
    iloIps = Array.apply(null, Array(serverSpec.cnt)).map(function (_, i) { return 'iloIp:' + i })
    dataIps1 = Array.apply(null, Array(serverSpec.cnt)).map(function (_, i) { return 'dataIp1:' + i })
    dataIps2 = Array.apply(null, Array(serverSpec.cnt)).map(function (_, i) { return 'dataIp2:' + i })
    dataIps = [dataIps1, dataIps2]
    serverIps = {};
    serverIps.ilo = iloIps;
    serverIps.dataIp = dataIps;

    domain = input.domainName.value;
    let racks = rack(serverSpec, networkSpec, serverIps, switchIps, domain);
    let cost = costing(networkSpec, serverSpec);
    return { server: serverSpec, network: networkSpec,racks , totalCPU: totalCPU , totalRAM: totalRAM , totalHadoop: totalHadoop, totalNfs: totalNfs, totalElastic: totalElastic,cost:cost };
};
