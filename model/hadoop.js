const hadoop = function () {
	const hdd = function () {
		return hdfsSize() + hbaseSize();
	};
	const cpu = function () {
		const totalRegionCount = hbaseSize() / hBasePerRegionSize;
        const cpuDfe = totalRegionCount ;
        const cpuCb = totalRegionCount * ((input.precisionMarketerYN.value ? (input.noBC.value / 2000) :0 ) + (input.wbYN.value ? input.imSecOffers.value / 500 : 0)) ;
        const cpuDk = totalRegionCount  * 0.5 ;
        const cpuHadoop = 18 + (totalRegionCount * 0.25);
		return cpuDfe + cpuCb + cpuDk + cpuHadoop;
	};
	const ram = function () {
		const totalRegionCount = hbaseSize() / hBasePerRegionSize;
        const memDfe = totalRegionCount *  2;
        const memCb = totalRegionCount * (input.precisionMarketerYN.value ? 6 :0);
        const memDk = totalRegionCount * Math.ceil(input.feedSize.value / 1024) * 5	;
        const memHadoop = 128 + (totalRegionCount * 1);
		return memHadoop + memDk + memCb + memDfe;
	};

	const hbaseSize = function () {
		retensionOfEvents = input.retensionOfEvents.value ;
		consumerBaseTemp = input.retensionOfEvents.value * sizePerEvent * input.consumerBaseMi.value;
		tsSize = input.precisionMarketerYN.value ? consumerBaseTemp + input.noOfMessages.value : 1;
		matrixSize = input.precisionMarketerYN.value ? consumerBaseTemp + (input.consumerBaseMi.value * conversionRate) + ((noOfDays + weeklyRetension + monthlyRetension) * sizePerEvent * (input.consumerBaseMi.value / mbSize) * (input.noAttributes.value / 250)) : 1;
		imSeqSize = (input.imYN.value) ? input.consumerBaseMi.value * input.imSecOffers.value / 500 * 3 : 0;

		// event Size calc
		ackHbase = input.precisionMarketerYN.value ? 112 * input.noOfMessages.value * retensionOfEvents : 1;
		sysEvents = input.precisionMarketerYN.value ? ackHbase * .5 : 1;
		custEventsHbase = input.precisionMarketerYN.value ? input.consumerBaseMi.value * input.retensionOfEvents.value * 150 * 0.25 : 1 ;
		eventSize =  ackHbase + sysEvents +  custEventsHbase;
		// event size calc ends 

		iajSize = input.wbYN.value ? (input.consumerBaseMi.value / fiveMi + input.wbIajJobExpected.value / 40) * 40 : 0;
		cgSize = input.tgCgRequiredYN.value ? (input.consumerBaseMi.value / fiveMi + input.noBC.value * .0025) * 40 : 0;
		modelSize = input.wbYN.value ? input.wbExpectedModels.value * (input.consumerBaseMi.value / mi) * 10 : 0;
		wbSize = iajSize + cgSize + modelSize;
		totHbaseSize = Math.ceil((tsSize + matrixSize + imSeqSize + wbSize + eventSize) * input.replicationFactor.value /gbSize);

		return (!totHbaseSize) ? 1: totHbaseSize ;
	};
	const hdfsSize = function () {
		dfeSize = input.feedSize.value * 3 * input.replicationFactor.value;
		logSize = input.noAttributes.value * 24 * 50 * input.logRetensionPeriod.value/1024;      //50 MB
		
		ackParq = input.precisionMarketerYN.value ? 1.5 * 8 * input.noOfMessages.value * retensionOfEvents : 1;
		conParq = input.precisionMarketerYN.value ? 29 * input.noOfMessages.value * retensionOfEvents : 1;		
		regParq = input.precisionMarketerYN.value ? input.consumerBaseMi.value * 48 : 1;
		cpParq = input.precisionMarketerYN.value ? regParq * 170 : 1;
		return Math.ceil((dfeSize * 2) + logSize + ((ackParq + conParq + regParq + cpParq)/gbSize ));
	};
    return {hdd:hdd, cpu:cpu, ram:ram, hdfsSize:hdfsSize, hbaseSize:hbaseSize };
}();