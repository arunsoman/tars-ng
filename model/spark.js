spark = function () {
    const hdd = function () {
        const hddIAJ = input.wbYN.value ? (input.consumerBaseMi.value / fiveMi) + (input.wbIajJobExpected.value / 40) * 50 : 0;
        const hddCG = input.tgCgRequiredYN.value ? (input.consumerBaseMi.value / fiveMi) + (input.noBC.value * 0.1 / 40) * 50 : 0;
        const hddModel = input.wbYN.value ? input.wbExpectedModels.value * 10 * input.consumerBaseMi.value / mi : 0;
        return hddIAJ + hddCG + hddModel;
    };
    const cpu = function () {
        const cpuIAJ = input.wbYN.value ? (input.consumerBaseMi.value / fiveMi) * (input.wbIajJobExpected.value / 40) * 4 : 0;
        const cpuCG = input.tgCgRequiredYN.value ? (input.consumerBaseMi.value / (fiveMi * 2)) * (input.noBC.value * 0.1 / 25) * 4 : 0;
        const cpuModel = input.wbYN.value ? input.wbExpectedModels.value * 16 * input.consumerBaseMi.value / (fiveMi * 4) : 0;
        const cpuConsumerProfile =  (input.consumerBaseMi.value < fiveMi) ?4: (input.consumerBaseMi.value / mi * .4) + 4;
        return cpuConsumerProfile + cpuIAJ + cpuCG + cpuModel;
    };
    const ram = function () {
        const ramIAJ = input.wbYN.value ? (input.consumerBaseMi.value / fiveMi) * (input.wbIajJobExpected.value / 40) * 16 : 0;
        const ramCG = input.tgCgRequiredYN.value ? (input.consumerBaseMi.value / (fiveMi * 2)) + (input.noBC.value * 0.1 / 25) * 16 : 0;
        const ramModel = input.wbYN.value ? input.wbExpectedModels.value * 32 * input.consumerBaseMi.value / (fiveMi * 4) : 0;
        const ramConsumerProfile = (input.consumerBaseMi.value < fiveMi)?8
        :(input.consumerBaseMi.value / mi * .8) + 8;
        return ramConsumerProfile + ramIAJ + ramCG + ramModel;
    };
    return {hdd:hdd, cpu:cpu, ram:ram};
}();