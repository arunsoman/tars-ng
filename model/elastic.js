elastic = function () {

    const hdd = function () {
        return 100 + Math.ceil(input.consumerBaseMi.value / 1000000 * 85) * 2;

    };
    const cpu = function () {
        if (input.consumerBaseMi.value < MinSUB){
            return 10;
        }
        return 10 + Math.ceil(input.consumerBaseMi.value / 1000000 * 2);
    };
    const ram = function () {
        if (input.consumerBaseMi.value < MinSUB) {
            return 20;
        }
        return 20 + Math.ceil(input.consumerBaseMi.value / 1000000 * 4);
    };
    return {hdd:hdd, cpu:cpu, ram:ram};
}();