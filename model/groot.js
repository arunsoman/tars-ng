var groot = function (nodes) {
    var hdd = () => nodes * 30;

    var cpu = () => ((nodes - 1) * 0.5 + 2 );

    var ram = () =>  (nodes * 0.1 ) ;

    return {hdd:hdd, cpu:cpu, ram:ram};
};