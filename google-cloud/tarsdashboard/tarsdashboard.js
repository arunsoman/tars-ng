exports.tarsdashboard = function dashboard(req, res) {

    var crypto = require('./crypto');
    var jsonEncrypted = req.query.token;
    try {
        var id = -1;
        if ('id' in req.query) {
            id = req.query.id;
        }
        var json = crypto.decrypt(jsonEncrypted);
        var jsonObject = JSON.parse(json);
        var email = jsonObject.email;
        res.setHeader("Content-Type", "text/html");
        res.status(200).send(`<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <link rel="stylesheet" href="https://storage.googleapis.com/tars-site/style.css">
</head>

<body ng-app="myApp" ng-controller="app">
<script src="https://storage.googleapis.com/tars-site/import.js"></script>
    <!-- <script src="scripts/angular.min.js"></script>
    <script src="https://storage.googleapis.com/tars-site/scripts/angular-animate.js"></script>
        <script src="https://storage.googleapis.com/tars-site/scripts/app.js"></script>
    <!-- <script src="input.js"></script> -->
    <div class="main-content">

        <div class="content-wrapper">
            <div>
                <!-- <div class="rack-view scale-1" ng-if="racks" ng-repeat="rack in racks track by $index">
                <ul>
                    <li class="rack" ng-repeat="server in rack">
                        <div ng-class="{'server':server.rack}" class="in">{{rack.index}}</div>
                        <span class="index">{{rack.length- $index}}</span>
                        <span class="index after">{{rack.length- $index}}</span>
                    </li>
                </ul>
            </div> -->
                <div class="perspective" ng-if="racks.length">
                    <div class="shape-wrapper" ng-class="{'spin':spin}">
                        <div class="shape contain shape-{{$index}}" ng-repeat="rack in racks track by $index">
                            <span class="ft"></span>
                            <span class="rt"></span>
                            <span class="bk"></span>
                            <span class="lt"></span>
                            <span class="tp"></span>
                            <span class="bm"></span>
                            <div class="shape {{server.meta.meta}}" ng-repeat="server in rack" ng-class="{'contain':server.rack,'switch':server.switch}">
                                <span class="ft">
                                    <i ng-if="server.meta">{{server.meta.meta}}</i>
                                </span>
                                <span class="rt"></span>
                                <span class="bk"></span>
                                <span class="lt"></span>
                                <span class="tp"></span>
                                <span class="bm"></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="desc" ng-if="!racks.length">

                    <h1>Empty rack for you</h1>
                    <p>Lets add servers and switches to power up</p>
                </div>
            </div>
        </div>
    </div>
    <aside>
        <div class="aside-wrapper">
            <h2>TARS NG</h2>
            <div class="nav-wrapper">
                <div class="hambergur">
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
                <ul class="nav">
                    <li>
                        <a href="#" ng-click="save()">Save</a>
                    </li>
                    <li>
                        <a href="settings">Settings</a>
                    </li>
                    <li>
                        <a href="logout.html">Logout</a>
                    </li>
                </ul>
            </div>

            <div>
                <ul>
                    <li ng-repeat="field in input track by $index">
                        <label>{{field.label}}</label>
                        <input type="text" ng-model="field.value">
                    </li>
                </ul>

            </div>
        </div>
    </aside>
    
</body>

</html>`);

    } catch (err) {
        res.redirect('/tarslogin?signout=1');
    }

};
