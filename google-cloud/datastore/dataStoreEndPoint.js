const dataStoreObj = require('./DataStore.js');

exports.dataStoreEndPoint = function endPoint(req, res) {
	var crypto = require('./crypto');
	var jsonEncrypted = req.query.token;
	try {
		var json = crypto.decrypt(jsonEncrypted);
		var jsonObject = JSON.parse(json);
		var email = jsonObject.email;
	
		if (req.method == 'GET') {
			invokGetMethod(req,res);
		}

	if (req.method == 'POST') {
		
		var project = req.body.projectName;
		var state = req.body.projectState;
		var version = req.body.docVersion;
		var lmTime = new Date();
		var inputJson = req.body.inputData;
		var createdBy = email;
		var modifiedBy = email;

		if (mandatoryCheck(res,project,'Project Name') && mandatoryCheck(res,state,'Project State')){

			childQuery = getSimpleQuery('projectName',String(project));
			dataStoreObj.getQueryResult(childQuery).then(results => {
				const entity = results[0];
 				if (entity.length > 0){
 					res.status(400).send(JSON.stringify({ "status": "error" ,"message":"Project Name already Exist."}));
 				}else{
 					dataStoreObj.saveRecord(project, state, version, lmTime,createdBy, modifiedBy,inputJson,'').then(() => {
						res.status(200).send(JSON.stringify({ "status": "ok" , "message":"Saved successfully"}));
					});	
 				}
			});	
		}			

	}

	if (req.method == 'DELETE') {
		var taskKey = req.query.taskKey;
		if (mandatoryCheck(res,taskKey,'Id')){
			childQuery = getSimpleQuery('parentId',String(taskKey),'status','NEW');
			dataStoreObj.getQueryResult(childQuery).then(results => {
					const entity = results[0];
	 				if (entity.length > 0){
	 					res.status(400).send(JSON.stringify({ "status": "error" ,"message":"This document is already versioned. Please remove the child document"}));
	 				}else{
	 					dataStoreObj.getRecord(taskKey).then(results => {
	 						const deletetRecord = results[0];
	 						if (deletetRecord) {
								dataStoreObj.markasDeleted(deletetRecord).then(() => {
									res.status(200).send(JSON.stringify({ "status": "ok" ,"message":"Record mark as Deleted."}));
								});
								
							}else{
								res.status(400).send(JSON.stringify({ "status": "error" ,"message":"Unable to delete the Record Please check the log"}));
							}
							
	 					}); 						
	 				}
				});
		}
		
	}

	if (req.method == 'PUT') {
		
		var project = req.body.projectName;
		var state = req.body.projectState;
		var version = req.body.docVersion;
		var lmTime = new Date();
		var inputJson = req.body.inputData;
		var createdBy = req.body.createdUser;
		var modifiedBy = email;
		var parentId = req.body.parentId;
		var taskId = req.body.taskId;
		if (parentId){
			version = (parseFloat(version) + 1.0).toFixed(1);
			query = dataStoreObj.datastore.createQuery('Task').filter('projectName', '=', project).filter('docVersion','=',version).order('projectName');
			dataStoreObj.getQueryResult(query).then(results => {
				const entity = results[0];
				if (entity.length > 0){
					res.status(400).send(JSON.stringify({ "status": "error" ,"message":"Project,Version combination already existing."}));					
 				}else{
 					mappedQuery = dataStoreObj.datastore.createQuery('Task').filter('parentId', '=', String(parentId)).order('parentId');
					dataStoreObj.getQueryResult(mappedQuery).then(results => {
						const entity = results[0];
	 					if (entity.length > 0){
	 						res.status(400).send(JSON.stringify({ "status": "error" ,"message":"Please use latest version for creating the new configuration."}));
	 					}else{
		 					dataStoreObj.saveRecord(project, state, version, lmTime, createdBy,modifiedBy,inputJson,parentId).then(() => {
							res.status(200).send(JSON.stringify({ "status": "ok" ,"message":"New version saved"}));	
							});			
	 					}
					});
 				}
			});
			

		}else{
			
			if (mandatoryCheck(res,taskId,'Id')){
				records = dataStoreObj.updateRecord(taskId, project, state, version, lmTime, createdBy,inputJson,'').then(() => {
					res.status(200).send(JSON.stringify({ "status": "ok" ,"message":"Record updated"}));
				});
			}
	    }
	}

	} catch (err) {
		res.redirect('/tarslogin?signout=1');
	}
};


function invokGetMethod(req,res){

		var historyOption = req.query.showHistory;
		if ( historyOption== 'NEXT'){

			var taskId= req.query.taskId;
			if(mandatoryCheck(res,taskId,'Task Id')){
				query = getSimpleQuery('parentId',String(taskId));
				sendNextResponse(res,query);
			}
						
						

		} else if (historyOption == 'PREVIOUS'){
			var parentId = req.query.parentId;
			if (mandatoryCheck(res,parentId,'Parent Id')){
				sendPreviousResponse(res,parentId);	
			}							

		}else{
			
			records = dataStoreObj.getAllRecords().then(results => {
				tasks = results[0];
				tasks.forEach(task => {
					const taskKey = task[dataStoreObj.datastore.KEY];
					task.id=taskKey.id;
				  });
				tasks = filterLowerVersions(tasks);
				res.status(200).send(tasks);

			});
		}

}

function getSimpleQuery(key,value){

	var queryObj =  dataStoreObj.datastore.createQuery('Task');
	for (var i = 0; i < arguments.length; i=i+2)
		queryObj.filter(arguments[i], '=', arguments[i+1]);

	return queryObj.order(key);
}


function sendNextResponse(res,query)	{
	dataStoreObj.getQueryResult(query).then(results => {
				const entities = results[0];
				if (entities.length > 0){
					entities.forEach(entity => {
						const taskKey = entity[dataStoreObj.datastore.KEY];
						entity.id=taskKey.id;
		 			});
					res.status(200).send(entities[0]); 					
				}else{
					res.status(400).send(JSON.stringify({ "status": "warning" ,"message":"No Record Found"}));								
				}
			});	

}

function sendPreviousResponse(res,id){
	dataStoreObj.getRecord(id).then(results =>  {
				const task = results[0];
				if (task) {
					const taskKey = task[dataStoreObj.datastore.KEY];
					task.id=taskKey.id;
					res.status(200).send(task);
				}else{
					res.status(400).send(JSON.stringify({ "status": "warning" ,"message":"No Record Found"}));
				}
								
			});	
}

function mandatoryCheck(res,value,fieldName){	
	if (!value || String(value).length == 0){
		res.status(400).send(JSON.stringify({ "status": "error" ,"message":"Mandatory Field Missing - "+fieldName}));
		return false;
	}
	return true;		
}

function filterLowerVersions(entities){

	var projectNames = [];
	var results = [];
	if (entities && entities.length){
		for (var index = 0 ;index < entities.length ; index ++){
			var selectedEntity = entities[index];
			for (var subIndex = index +1 ; subIndex < entities.length ;subIndex++){				
				if ((selectedEntity.projectName == entities[subIndex].projectName ) && (parseFloat(selectedEntity.docVersion) < parseFloat(entities[subIndex].docVersion))){
					selectedEntity = entities[subIndex];					
				}
			}
			if(projectNames.indexOf(selectedEntity.projectName) < 0 ){				
				results.push(selectedEntity);
				projectNames.push(selectedEntity.projectName);
			}

		}
		
	}
	return results;	
}