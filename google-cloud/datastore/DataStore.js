const Datastore = require('@google-cloud/datastore');
const projectId = 'complete-land-188108';

const datastore = new Datastore({
		projectId: projectId,
});
exports.datastore=datastore;

exports.saveRecord = function (project,state,version,lmTime,createdBy,modifiedBy,inputJson,parentRecId){
	task = getEntity(project,state,version,lmTime,createdBy,modifiedBy,inputJson,parentRecId);
	const taskKey = datastore.key('Task');
	const entity ={
		key: taskKey,
		excludeFromIndexes: ['inputData'],
		data: task,
	};
	return datastore.insert(entity);
};

exports.getRecord = function (taskId) {
	const taskKey = datastore.key(['Task', parseInt(taskId)]);
	return datastore.get(taskKey); 
};

exports.getQueryResult = function (query) {
	return datastore.runQuery(query);
};


function getEntity(project,state,version,lmTime,createdBy,modifiedBy,inputJson,parentRecId){	
	task = {		
				projectName: project,
				projectState: state,
				docVersion:version,
				modificationTime:lmTime,
				createdUser : createdBy,
				modifiedUser : modifiedBy,
				inputData:inputJson,
				parentId : parentRecId,
				status:'NEW'

	};
	return task;
}

exports.deleteRecord = function  (taskId) {
		const taskKey = datastore.key(['Task', parseInt(taskId)]);
		return datastore.delete(taskKey);
};


exports.getAllRecords =function () {
	const query = datastore.createQuery('Task').filter('status','=','NEW');
	return datastore.runQuery(query);    			
};

exports.updateRecord = function (taskId,project,state,version,lmTime,createdBy,modifiedBy,inputJson,parentRecId){
	task = getEntity(project,state,version,lmTime,createdBy,modifiedBy,inputJson,parentRecId);
	const taskKey = datastore.key(['Task', parseInt(taskId)]);
	const entity ={
		key: taskKey,
		data: task,
	};
	return datastore.update(entity);
};

exports.markasDeleted = function (updateEntity){
	taskKey = updateEntity[datastore.KEY];
	dataEntity = getEntity(updateEntity.projectName,updateEntity.projectState,updateEntity.docVersion,updateEntity.modificationTime,updateEntity.createdUser,updateEntity.modifiedUser,updateEntity.inputData,updateEntity.parentId);
	dataEntity.status ='DELETED';
	const entity ={
		key: taskKey,
		excludeFromIndexes: ['inputData'],
		data: dataEntity,
	};
	return datastore.update(entity);
};
