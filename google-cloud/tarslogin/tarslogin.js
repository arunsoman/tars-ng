exports.tarslogin = function loginHtml(req, res) {
  var isSignOut = false;
  if ('signout' in req.query) {
    isSignOut = true;
  }
  res.setHeader("Content-Type", "text/html");
  res.status(200).send(`<html lang="en">
  <head>
    <meta name="google-signin-scope" content="profile email">
    <meta name="google-signin-client_id" content="310884155109-4bvcjo9r1njgmqdascm60r2jeuhtgf1o.apps.googleusercontent.com">
    <script src="https://apis.google.com/js/platform.js" async defer></script>
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <link rel="stylesheet" href="https://storage.googleapis.com/tars-site/style.css">

  </head>
  <body style="background: #2f76f8;">
  <h1 style="position: block;text-align:center;font-size:72px; font-family: 'Roboto';color:white;margin-top:48px;">TARS NG</h2>
    <div class="g-signin2" style="position: fixed;top: 50%;left: 50%;transform: translate(-50%, -50%);" data-onsuccess="onSignIn" data-prompt="select_account" data-theme="dark"></div>
    <script>
    var isSignOut=${isSignOut};
    var user='';
      function onSignIn(googleUser) {
        
        if(isSignOut)
        { var auth2 = gapi.auth2.getAuthInstance();
          auth2.disconnect();
          isSignOut=false;
          

        }else{
        // The ID token you need to pass to your backend:
        var id_token = googleUser.getAuthResponse().id_token;
        if(googleUser.getHostedDomain() !== undefined &&googleUser.getHostedDomain()==="flytxt.com")
        {
          window.location = "tarsauthorize?token="+id_token;
        }else
        {var auth2 = gapi.auth2.getAuthInstance();
           auth2.disconnect();
           alert("Since you are not a part of flytxt.com, you are not allowed to view this page")
        
          
         
        }
      }
      };
    </script>
  </body>
</html>`);
};
