exports.tarslist = function tarslist(req, res) {
    var crypto = require('./crypto');
    var jsonEncrypted = req.query.token;
    try {
        var json = crypto.decrypt(jsonEncrypted);
        var jsonObject = JSON.parse(json);
        var email = jsonObject.email;
        res.setHeader("Content-Type", "text/html");
    res.status(200).send(`<!DOCTYPE html>
    <html lang="en">
    
    <head>
        <title>List</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <script>
            $(document).ready(function () {
    
      $.get("dataStoreEndPoint", function (data) {
                
                var listData = "";
                for (i in data) {
                    listData += "<tr><td>" + data[i].projectName + "</td><td>" + data[i].projectState + "</td><td>" + data[i].inputData + "</td><td>" + data[i].docVersion + "</td><td><button class='btn btn-info btn-edit' data-id="+data[i].id+">edit</button> <button type='button' class='btn-delete btn btn-danger' data-id='" + data[i].id + "' data-name='" + data[i].projectName + "' data-toggle='modal' data-target='#myModal'>Delete</button></td></tr>";
                }
                $('#list').html(listData);
    
                $(document).on("click", ".btn-delete", function () {
                    $('#delete-text').html("Are you sure to delete " + $(this).data('name') + "?");
                    $('#btn-delete-confirm').attr('data-id', $(this).data('id'));
    
                });
                $(document).on("click", "#btn-delete-confirm", function () {
                    $.ajax({
                        url: '/dataStoreEndPoint?taskKey='+$(this).data('id')+"&"+window.location.search.substr(1),
                        type: 'DELETE',
                        success: function(result) {
                            // Do something with the result
                        }
                    });
                    $('#delete-text').html("Are you sure to delete " + $(this).data('name') + "?");
                    $('#btn-delete-confirm').attr('data-id', $(this).data('id'));
    
                });
                $(document).on("click", ".btn-edit", function () {
                    window.location="tarsdashboard?id="+$(this).data('id')+"&"+window.location.search.substr(1);
                  
    
                });
      });
            });
    
    
    
    
        </script>
    </head>
    
    <body style="background: #2f76f8;">
        <h1 style="position: block;text-align:center;font-size:48px; font-family: 'Roboto';color:white;margin-top:18px;">TARS NG</h2>
            <div class="container">
                <h3 style="background:#EEEEEE;padding:16px;border-radius: 4px;">List</h3>
                <table class="table table-hover" style="background:#EEEEEE;border-radius: 4px;">
                    <thead>
                        <tr>
                            <th>projectName</th>
                            <th>projectState</th>
                            <th>inputData</th>
                            <th>docVersion</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody id="list">
    
                    </tbody>
                </table>
            </div>
            <div class="modal fade" id="myModal" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Delete</h4>
                        </div>
                        <div class="modal-body">
                            <p id="delete-text"></p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" id="btn-delete-confirm" class="btn btn-danger" data-dismiss="modal">Delete</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
    </body>
    
    </html>`);
    } catch (err) {
        res.redirect('/tarslogin?signout=1');
    }

}