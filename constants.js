const sizePerEvent = 117 ;//BYTES
const mi = 1000000;
const fiveMi = 5 * mi;
const MinSUB = fiveMi;
const VOLTDB_INST=3;
const MAX_SERVERS_IN_RACK = 25;

const buffer = 3;
const stackcon = 4;
const RESERVED_PORTS = stackcon+buffer;
const conversionRate  = .1;
const noOfDays = 30;
const mbSize = 1024 * 1024;
const gbSize =  mbSize * 1024;
const monthlyRetension  = input.retensionOfEvents.value/30;
const weeklyRetension = input.retensionOfEvents.value/7;
const jobCompletionTime = 300;  // minutes
const hBasePerRegionSize = 30;   // in GB