var app = angular.module('myApp', []).config(function () {
});


var resetValues =JSON.parse(JSON.stringify(input));
app.controller('app', function ($scope, $http, $timeout) {
	$scope.token = window.location.search.replace('?token=', '');
	$http.defaults.headers.post["Content-Type"] = "application/json";
	$scope.IsListVisible = false;
	$scope.slots =42;
	$scope.editCheck = false;
	$scope.cableType = {
		'1g': 'CAT6',
		'10g':'SFP+ DAC'
	}
	$scope.toggle={
		'second':false,
		'nav':false,
		'rack':false,
		'list':false,
		'check':-1,
		'tab':0,
		'view':false,
		'panel':'home'
	}



	

	$scope.getDiskImages = function(item){
		$scope.diskImages = Object.keys(item).reduce((a, b) => {
			if (item[b].tag && item[b].tag.indexOf('hdd_')!=-1) {
				a[b] = item[b]
			}
			return a
		}, {});
	}
	$scope.getSwitches = function(){
		let mergedRacks = [].concat.apply([],$scope.racks);
		let totalSwitches = mergedRacks.map(item=>{if(item.ip) return item})
							.filter(item=>item!=undefined)
							.reduce((a,b)=>{
								a[b.name]=[];
								return a;
							},{});

		mergedRacks = mergedRacks.map(item => {
			if (item.data) return item
		}).filter(item => item != undefined);
		$scope.switches  = mergedRacks.reduce((a, b) => {
			b.data.forEach(item => a[item.sw.name].push(Object.assign({name:b.name},item)));
			a[b.ilo.sw.name].push(Object.assign({name:b.name},b.ilo));
			return a
		}, totalSwitches);
	}

	$scope.getPorts = function(n){
		$scope.ports = Array.from({length:n},(v,k)=>{
			return {id:k+1}
		})
	}

	
	$scope.save = function(){
		$http.post("https://us-central1-complete-land-188108.cloudfunctions.net/dataStoreEndPoint?token="+$scope.token,{
			inputData:JSON.stringify($scope.input),
			projectName:$scope.projectName,
			projectState: $scope.projectState,
			createdUser: $scope.createdUser,
			docVersion:"1.0"
		}).then((response)=>{
			alert('Saved successfully');
			$scope.list();
		})
	}
	$scope.create = function(){
		if(!$scope.projectName || !$scope.projectState){
			return false;
		}
		this.init();
		this.toggleView('home', 'panel');

	}
	$scope.update = function () {
		$http.put("https://us-central1-complete-land-188108.cloudfunctions.net/dataStoreEndPoint?token="+$scope.token, {
			inputData: JSON.stringify($scope.input),
			projectName: $scope.projectName,
			projectState: $scope.projectState,
			createdUser: $scope.editMeta.createdUser,
			parentId: $scope.editMeta.id,
			docVersion: $scope.editMeta.docVersion
		}).then((response) => {
			alert('Version saved successfully');
			$scope.list();
		})
	}
	$scope.edit = function(data){
		$scope.init();
		input = JSON.parse(data.inputData)
		$scope.input = input;
		$scope.getGroups();
		$scope.editCheck = true;
		$scope.editMeta = data;
		$scope.projectName = data.projectName;
		$scope.projectState = data.projectState;
		$scope.toggle.panel = false;
		// $scope.resetSearch();
	}
	$scope.toggleCheck = function(index){
		$scope.toggle.check = $scope.toggle.check == index ? null :index;
	}
	$scope.toggleTab = function(index){
		$scope.toggle.tab = index;
	}
	$scope.resetSearch = function(){
		$scope.search = null;
	}
	$scope.reset = function(hardReset){
		input = hardReset ? resetValues : defaultInput;
		$scope.input = input;
		$scope.editMeta = null;
		$scope.projectName = null;
		$scope.editCheck = false;
		$scope.getGroups();
		// $scope.resetSearch();
	}
	$scope.list = function () {
		$http.get("https://us-central1-complete-land-188108.cloudfunctions.net/dataStoreEndPoint?token="+$scope.token)
			.then(function (response) {
				$scope.dataList = response.data;

			});
	};

	$scope.getGroups = function(){
		$scope.inputGroups = Object.keys($scope.input).reduce((a, b) => {
			if (!a[$scope.input[b].groupId])
				a[$scope.input[b].groupId] = [];
			a[$scope.input[b].groupId].push($scope.input[b])
			return a;
		}, {});
	};
	$scope.getHistory = function(type){
		let param;
		let id;
		if (type == 'NEXT'){
			param = 'taskId';
			id = $scope.editMeta.id;
		}else{
			param = 'parentId';
			id = $scope.editMeta.parentId;
		}
		$http.get(" https://us-central1-complete-land-188108.cloudfunctions.net/dataStoreEndPoint?token="+$scope.token+"&showHistory=" + type + "&" + param+"="+id+" ")
			.then(function (response) {
				$scope.loading = false;
				if (response.data.docVersion){
					$scope.edit(response.data);
				}
			},function(){
				$scope.loading = false;
				if(type=='NEXT'){
					$scope.editMeta.id =null;
				}else{
					$scope.editMeta.parentId =null;
				}
			});
		$scope.loading = true;
	};

	$scope.toggleView = function(name,panel){
		$scope.toggle[panel] = $scope.toggle[panel] ==name ? null : name;
	};
	$scope.print = function(){
		this.toggle.view=true;
		setTimeout(() => {
			window.print();	
		});
	};

	$scope.init = function () {
		$scope.input = input;
		var defaultInput = input;
		$scope.emptySlots = Array.from({ length: 42 }, (v, i) => { return { id: i } }).reverse();
		$scope.isItemExist = function (index, id) {
			return $scope.racks[index].find(item => { return item.rackSlot == id });
		}
		$scope.$watch('input', (newVal, oldVal) => {
			var serverMeta = dx();
			// input = newVal;
			console.log(serverMeta.server.cnt, serverMeta.server.racks, serverMeta);
			let racks = serverMeta.racks;
			console.log(racks);
			$scope.racks = racks;
			$scope.getSwitches();
			$scope.getPorts(48);
			$scope.BOM = serverMeta.server;
			$scope.NBOM = serverMeta.network;
			$scope.cost = serverMeta.cost;
			$scope.getDiskImages($scope.BOM);


		}, true);
		$scope.getGroups();
		$scope.toggle.view = "view";
		$timeout(function() {
			$scope.toggle.view = null;
		},200);
	};
	$scope.list();
	

});


app.directive('numberOnly', function () {
	return {
		require: 'ngModel',
		restrict: 'A',
		scope: {
			numberOnly: '@'
		},
		link: function (scope, element, attrs, modelCtrl) {
			scope.$watch('numberOnly',function(value){
				modelCtrl.attr=value || 99999;
			});
			modelCtrl.$parsers.push(function (inputValue) {
				if (inputValue == null) {
					return '';
				}
				var cleanInputValue = inputValue.replace(/\D/gi, '').substr(0,modelCtrl.attr);
				if (cleanInputValue != inputValue) {
					modelCtrl.$setViewValue(cleanInputValue);
					modelCtrl.$render();
				}

				return (+cleanInputValue);
			});
		}
	}
});

app.filter('firstFilter', [function () {
	return function (string) {
		if (!angular.isString(string)) {
			return string;
		}
		return string.split('.')[0]
	};
}])
app.filter('getArray', [function () {
	return function (obj) {
		if(obj) return Object.keys(obj)
	};
}])

app.filter('percentageCompleted', [function () {
	return function (obj) {
		if(obj)		return (obj.map(item => item.value != '').filter(item => item).length / obj.length )*100
	};
}])
app.filter('splitBySpace', [function () {
	return function (string) {
		if(!string) return;
		return string.split(' ')
	};
}])
app.filter('splitByHyphen', [function () {
	return function (string) {
		if(!string) return;
		return string.split('-')
	};
}])

app.filter('splitItem', [function () {
	return function (string) {
		return string.split(' ')[1]
	};
}])

