// var mods = ['dx.js', 'elastic.js', 'groot.js', 'hadoop.js', 'input.js', 'mesos.js', 'mysql.js', 'spark.js', 'voltdb.js'];
var mods = ['input.js', 'networkManager.js', 'scripts/angular.min.js', 'scripts/angular-animate.js','scripts/app.js'];

(function (doc) {
    var appendScripts = function (srcs) {
        if (src = srcs.shift()) {
            var scriptTag = doc.createElement('SCRIPT');
            scriptTag.src = src;
            scriptTag.onload = function () { appendScripts(srcs) };
            doc.body.appendChild(scriptTag);
        }
    }
    // var goodBrowser = function () {
    //     // Return true if browser passes all feature tests
    //     return true;
    // }
    // if (goodBrowser()) {
        appendScripts(mods);
    // }
})(document);

// var imported = document.createElement('script');
// mods.forEach(function (mod) {
//     imported.src = mod;
//     document.head.appendChild(imported);
// })