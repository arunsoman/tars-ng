rack=function (sbom, nbom, serverIps, switchIps, domain) {
    const racks = [];
    const servers = [];
    const startStopIndex =[];
    const powerPerRack =[];

    const rackServers = function () {
        const serverGroupSize = 5;
        const maxServerGroup = MAX_SERVERS_IN_RACK / serverGroupSize;
        const startRackingFrom = 6;
        let slotId = startRackingFrom;
        let serverCntr = 0;
        for (let rackId = 0; rackId < sbom.racks; rackId++){
            racks[rackId]=[];
            let startIndex = serverCntr;
            for (let serverGroupId = 0; serverGroupId < maxServerGroup; serverGroupId++) {
                for (let sgsI = 0; sgsI < serverGroupSize; sgsI++) {
                    const server = {
                        name: 'node' + (serverCntr + 1) + '.'+ domain,
                        iloIp: serverIps.ilo[serverCntr],
                        dataIp: [
                            serverIps.dataIp[0][serverCntr],
                            serverIps.dataIp[1][serverCntr]
                        ],
                        rackId: rackId,
                        rackSlot: slotId
                    };
                    servers.push(server);
                    racks[rackId].push(server);
                    slotId++;
                    serverCntr++;
                    if(serverCntr===sbom.cnt){
                        startStopIndex.push({start:startIndex, stop:serverCntr});
                        return
                    }
                }
                slotId++;//air flow
            }
            slotId = startRackingFrom;
            startStopIndex.push({start:startIndex, stop:serverCntr})
            startIndex = serverCntr;
        }
    }();

    var switches = [];
    switches[0] = [] ;
    switches[1] = [] ;
    switches[2] = [] ;
    const totalSw = cnt = (nbom.cnt.split)?nbom.cnt.split(" ").map(a=>parseInt(a)).reduce((q,b)=>q+b):nbom.cnt;
    const rackSwitch = function () {
        const hybrid = nbom.tag.split(" ").length>0;
        const switchGroupSize = 1;
        const maxServerGroup = hybrid?3:2;
        const startRackingFrom = 6+25+5;
        let slotId = startRackingFrom;
        let switchCntr = 0;
        for (let rackId = 0; rackId < racks.length; rackId++){
            if (rackId % 2 === 1){
                if(servers.length-1 > ((rackId+1)*25)){
                    rackId++;
                }
            }

            for (let swichGroupId = 0; swichGroupId < maxServerGroup; swichGroupId++) {
                for (let sgsI = 0; sgsI < switchGroupSize; sgsI++) {
                    const aSwitch = {
                        name: 'sw' + (switchCntr+1) +'.'+ domain,
                        ip: [
                            switchIps[switchCntr],
                        ],
                        rackId: rackId,
                        rackSlot: slotId
                    };
                    racks[rackId].push(aSwitch);
                    switches[swichGroupId].push(aSwitch);
                    slotId++;
                    switchCntr++;
                    if(switchCntr === totalSw)
                        return;
                }
                slotId++;//air flow
            }
            slotId = startRackingFrom;
        }
    }();

    const connect = function () {
        //switches=getThemAll();
        let iLoSws= switches[2];
        let dataSws1 = switches[0];
        let dataSws2 = switches[1];
        if(iLoSws){
            iloC = Math.ceil(sbom.cnt/iLoSws.length);
            dataC = Math.ceil(sbom.cnt/dataSws1.length);

            let serverIndex = 0;
            iLoSws.forEach(function (aSw) {
                for(let i =0; i<iloC&&servers.length > serverIndex;i++){
                        servers[serverIndex++].ilo={
                            port:i+1,
                            sw:aSw
                        }
                }
            });

            serverIndex = 0;
            dataSws1.forEach(function (aSw, index) {
                for(let i =0; i<dataC&&servers.length > serverIndex;i++){
                        servers[serverIndex++].data=[
                            {
                                port:i+1,
                                sw:aSw
                            },{
                                port:i+1,
                                sw:dataSws2[index]
                            }]
                }
            })
        }
        else {
            const hybridSwitches=function (switchRef, noOfPorts) {
                let portsUsed =0;
                return (n)=>{
                    let available = Math.min(n, noOfPorts-portsUsed);
                    return Array.apply(null, Array(available)).
                    map((p)=>{return{port:portsUsed++, ref:switchRef}})
                };
            };
            const port = nbom.tag.split(" ")[0].split('-')[1];
            const usable = port - (buffer + stackcon);

            let hSwitches1 = switches[0].map(aSw=>{return hybridSwitches(aSw,usable)});
            let hSwitches2 = switches[1].map(aSw=>{return hybridSwitches(aSw,usable)});
            let nextHS1 = 0;
            let nextHS2 = 0;
            startStopIndex.forEach(function (ssI) {
                    let start = ssI.start;
                    let stop = ssI.stop;
                    for(let i = start; i < stop; i++){
                        let hSw1 = hSwitches1[nextHS1](1);
                        if(hSw1.length === 0)
                            hSw1 = hSwitches1[nextHS1++](1);
                        let hSw2 = hSwitches2[nextHS2](1);
                        if(hSw2.length === 0)
                            hSw2 = hSwitches2[nextHS2++](1);
                        let iHSw = (i < (start+stop)/2)?hSwitches1[nextHS1](1):hSwitches2[nextHS2](1);
                        if(iHSw.length ===0)
                            iHSw= (i < (start+stop)/2)?hSwitches1[nextHS1](1):hSwitches2[nextHS2](1);
                        let aServer = servers[i];

                        aServer.ilo={
                            port:iHSw[0].port+1,
                            sw:iHSw[0].ref
                        };
                        aServer.data=[
                            {
                                port:hSw1[0].port+1,
                                sw:hSw1[0].ref
                            },{
                                port:hSw2[0].port+1,
                                sw:hSw2[0].ref
                            }];
                    }
                }
            );
        }
    }();
    rackPower = function () {
        let serverCount = sbom.cnt ;
        let rackCount = sbom.racks ;

        const totalPower = {totalPower : ( serverCount * 430 ) +  (rackCount * 1050) } ;
        

        for (let rk = 1; rk <= rackCount ; rk++) {
            if (rk===rackCount){
                let RackSvr = (serverCount % 25) ;
                psCount = Math.ceil(((RackSvr + 3) * 450)/7000) *2;

                rkPwr = {
                name : 'rackpower' + (rk),
                power : (RackSvr + 3) * 450,
                psCount : psCount + ' x 7KW',
                pduCount : psCount  + ' x 15P Vertical PDUs'

                };
            }
            else{
                psCount = Math.ceil(12600/7000) *2 ;
                rkPwr = {
                    name : 'rackpower' + (rk),
                    power : 28*450,
                    psCount : psCount + ' x 7KVA',
                    pduCount : '4 x 15P Vertical PDUs'

                };
            }
            powerPerRack.push(rkPwr);
        }
        racks.power = [];
        racks.power.push(powerPerRack);
        racks.power.push(totalPower);
        
    }();

    return racks ;
}