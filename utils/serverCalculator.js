serverCalculator = function (totalCPU, totalMemory, totalHadoop, totalNfs, totalElastic) {
    const commodity = function (name, slots, options, sort) {
        if(!sort){
            sort = (a,b)=> (a<b)? -1:(a>b)?1:0;
        }
        const sortedOptions = options.slice().sort(sort);
        //debugger;
        return (q) => {
            let goodOption ={tag:name};
            sortedOptions.find(function(opt){
                if(q<opt){
                    goodOption.slot = 1;
                    goodOption.option = opt;
                    goodOption.cnt = 1;
                    return true;
                }
                else if(q< opt*slots){
                    let tt = Math.ceil(q/opt);
                    goodOption.slot = (tt < slots)? tt:slots;
                    goodOption.option = opt;
                    goodOption.cnt = 1;
                    return true;
                }
            });
            if(!goodOption.cnt){
                goodOption.cnt = Math.ceil(q/(sortedOptions[sortedOptions.length-1]*slots));
            }
            goodOption.distribute = function(distributeAmong){
                result =(goodOption.cnt < distributeAmong) ? distributeAmong : goodOption.cnt;
                result1 = (result*slots*sortedOptions[0] > q) ? Math.ceil(q / (result*sortedOptions[0])) : slots ;
                   return{
                        tag: name,
                        cnt: result,
                        slot: result1,
                        option: sortedOptions.find(
                            (x)=>
                            result*slots*x > (q))
                    }
            };

            return goodOption;
        };
    };

    const serverTemplate1 = {
        cpu: commodity("cpu",2, [6,8,12,16,20,28]),
        memory: commodity("memory",16, [8,10,12,16,20,24,32]),
        osHdd: commodity("hdd_1_os RAID1",2,[300]),
        hadoopHdd: commodity("hdd_4_hadoop NO_RAID",6, [300,600,900,1200,1800]),
        nfsHdd: commodity("hdd_2_nfs NO_RAID",1, [300,600,900]),
        elasticHdd: commodity("hdd_3_elastic NO_RAID",1, [300,600,900])
    };
    const raidutil = function(options)  {
        const raid5usable = (diskSize, diskCount) => (diskCount - 1) * diskSize;
        let viable;
        return function (holder, x) {
            Array.apply(null,
                Array(7)).map((_, i) => 3 + i).forEach((i) => {
                viable = options.find((y) => raid5usable(y, i) > x);
                if (viable){
                    holder.option= viable;
                    holder.slot= i;
                    return holder;
                }
            });
            return holder;
        };
    }([300,600,900]);
    const serverFactory = function (template) {
        const templates = [
                template.cpu(totalCPU),
                template.memory(totalMemory),
                template.hadoopHdd(totalHadoop),
                template.nfsHdd(totalNfs),
                template.elasticHdd(totalElastic)
            ];
        let servers =  templates.reduce((a,b)=>(a.cnt > b.cnt)? a : b);
        let serverCnt=(servers.cnt < 3)?3:servers.cnt;
        const withGroot = [
            template.cpu(totalCPU+(serverCnt*2)+groot(serverCnt).cpu()),
            template.memory(totalMemory+(serverCnt*10)+groot(serverCnt).ram()),
            template.hadoopHdd(totalHadoop),
            template.nfsHdd(totalNfs+groot(serverCnt).hdd()),
            template.elasticHdd(totalElastic)
        ];
        servers =  withGroot.reduce((a,b)=>(a.cnt > b.cnt)? a : b);
        serverCnt=(servers.cnt < 3)?3:servers.cnt;
        const distribution = withGroot.map(x=> x.distribute(serverCnt));
        const result = {cnt:serverCnt, racks:Math.ceil(serverCnt/MAX_SERVERS_IN_RACK) };
        distribution.forEach(x=> result[x.tag]=x );
        result.os = {tag: "hdd_1_os RAID1", slot: 2, option: 300};
        livehdd = raidutil({tag: "hdd_2_jarvislive RAID5"}, input.feedSize.value *2);
        archdd = raidutil({tag: "hdd_3_jarvisarch RAID5"}, input.feedSize.value * input.retensionOfftp.value * 0.25);

        result.jarvis= {
        jarvisOshdd: {tag: "hdd_1_jarvisOs RAID1", slot: 2, option: 600},
        jarvishddlive: (livehdd.slot||livehdd.option) ? livehdd : {tag: "hdd_2_jarvislive RAID5", slot: "SAN", option: input.feedSize.value *2},
        jarvishddarch : (archdd.slot||archdd.option) ? archdd : {tag: "hdd_3_jarvisarch RAID5", slot: "SAN", option: input.feedSize.value * input.retensionOfftp.value * 0.25}
        };
        return result;
    };
    return serverFactory(serverTemplate1);
};