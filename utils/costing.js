const costing = function(nbom, sbom) {
    const serverCost = function(serverCnt, core) {

        costMetrics = {
            12: 6800,
            16: 9800,
            24: 10200,
            32: 16800,
            40: 20500,
            56: 25500
        }
        return serverCnt * costMetrics[core];
    }
    const switchCost = function(switchCnt, mode, port) {

        costMetrics = {
            g1: 3500,
            g10: 11500
        }
        return switchCnt * (port == 24 ? costMetrics[mode] * 0.65 : costMetrics[mode]);
    }
    const cableCost = function(cat) {
        costMetrics = {
            g10: 160,
            g1: 25
        }
        return cat.split(/-| /).filter((a, i) => i === 0 || i === 2).map(
            (a, i) => a * costMetrics[i === 0 ? "g1" : "g10"]).reduce(
            (a, b) => a + b);
            
        // precheck = cat.split(" ");
        // g1count = precheck[0].split("-")[0]
        // g10count = precheck[1].split("-")[0]

        // return ( g1count * costMetrics["g1"] ) + (g10count * costMetrics["g10"]);

    }
    const rackCost = function(rackcnt) {
        var cost = 3500;
        return rackcnt * cost;
    }
    const softLicenseCost = function(serverCnt, software) {
        costMetrics = {
            linux: 810,
            volt: 0,
            mysql: 3000
        }
        //  Object.entries(costMetrics).reduce ((a,b) => ["",a[1] + b[1]]  );
        return serverCnt * costMetrics[software];
    }

    meta = {
        serverCost: serverCost(sbom.cnt, sbom.cpu.slot * sbom.cpu.option),
        switchCost: parseInt(nbom.cnt.toString().split(" ").map((s, i) => switchCost(s, (i === 0) ? "g1" : "g10", nbom.tag.split(" ")[i].split("-")[1])).reduce((a, b) => a + b)),
        cableCost: cableCost(nbom.cat),
        rackCost: rackCost(sbom.racks),
        softLicenseCost: [{
                cnt: sbom.cnt,
                software: "linux"
            },
            {
                cnt: 3,
                software: "volt"
            },
            {
                cnt: 1,
                software: "mysql"
            }
        ].map((a, b) => softLicenseCost(a.cnt, a.software)).reduce((a, b) => a + b)
    }

    totalCost = Object.entries(meta).reduce((a, b) => ["totalCost", a[1] + b[1]]);
    meta.totalCost = totalCost[1]

    return meta;

};