const networkManager = function (serverCnt, sub, fSize, domain, iloIps, dataIps) {
    const gw = 4;
    const extra = 4;
    const nSwitch = function (port, speed, cost, tag) {
        const usable = port - (buffer + stackcon);
        return function (cnt) {
            return {
                tag: tag,
                cnt: Math.ceil(cnt / usable),
                cost: Math.ceil(cnt / usable) * cost,
                usable:usable,
                freePorts: usable - cnt % usable,
                speed: speed + "g",
                add: function() {
                    this.cnt = this.cnt+1;
                    this.cost = this.cost*2;
                    this.usable = this.usable*2;
                    this.freePorts = this.freePorts*2;
                }
            };
        };
    };
    const s241g = nSwitch(24, 1, 1, "s-24-1g");
    const s2410g = nSwitch(24, 10, 1.7, "s-24-10g");
    const s481g = nSwitch(48, 1, 1.6, "s-48-1g");
    const s4810g = nSwitch(48, 10, 2.5, "s-48-10g");

    const serverStat = function (cnt) {
        return {
            ilo: 1 * cnt,
            nic: 2 * cnt,
            total: cnt + 2 * cnt
        };
    }(serverCnt);

    const all1g24 = s241g(serverStat.total);
    const all1g48 = s481g(serverStat.total);
    const combo = function () {
        const reducer = function (a, b) {
            return {
                tag: a.tag + " " + b.tag,
                cnt: a.cnt + " " + b.cnt,
                cost: a.cost + b.cost,
                usable: a.usable +" " + b.usable,
                freePorts: a.freePorts + " " + b.freePorts,
                speed: a.speed + " " + b.speed
            };
        };
        return Array.prototype.slice.call(arguments).reduce(reducer);
    };

    const ilo1g24 = s241g(serverStat.ilo);
    const ilo1g48 = s481g(serverStat.ilo);
    const nic10g24 = s2410g(serverStat.nic);
    const nic10g48 = s4810g(serverStat.nic);

    const minCost = function () {
        const minimize = (a, b) => {
            return (a.cost < b.cost) ? a : b
        };
        return Array.prototype.slice.call(arguments).reduce(minimize);
    };

    const all1g = function () {
        const tt = minCost(all1g24, all1g48);
        if(tt.cnt % 2 === 1 ) tt.cnt += 1;
        tt.cat = serverCnt * 3 + gw + extra + " 1g";
        tt.serverNicType="2 X 1G";
        tt.dataIPs=serverCnt * 2+7;
        tt.MgmtIPs=serverCnt + 2;
        return tt;
    };

    const hybrid = function () {
        const ilo = minCost(ilo1g24, ilo1g48);
         if (nic10g48.cnt === 1) nic10g48.add();
        const nic = minCost(nic10g24, nic10g48);
        if(nic.cnt % 2 === 1) nic.cnt += 1;
        const tt = combo(ilo, nic);
        tt.serverNicType="2 X 10G";
        tt.cat = ((serverCnt + gw + extra) + "-1g ") + (serverCnt * 2 + gw + extra) + "-10g";
        tt.dataIPs=serverCnt * 2+7;
        tt.MgmtIPs=serverCnt + 2;
        return tt;

    };
    return ((sub > 5000000 || fSize > 50) ? hybrid : all1g)();
};