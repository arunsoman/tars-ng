var input = {
	consumerBaseMi: { label: "Consumer Base (Million)", value: 130000000, maxLength: 9, groupId: "Big Data"},
	feedSize: { label: "Feed Size (GB)", value: 470, maxLength: 5, groupId: "Big Data" },
	noAttributes: { label: "No. Of Attributes", value: 500, maxLength: 4, groupId: "Big Data" },
	retensionOfEvents: { label: "Retension Of Events", value: 90, maxLength: 3, groupId: "Big Data" },    // consumerBase - converted to millon
	retensionOfftp: { label: "Retension Of FTP (in Days)", value: 7, optional: true, maxLength: 2, groupId: "Big Data" },
	precisionMarketerYN: { label: "Precision Marketer Enabled", value: true, groupId: "Precision Marketer",select:true },
	imYN: { label: "IM Enabled", value: true, groupId: "Intent Management", select: true },
	wbYN: { label: "WB Enabled", value: true, groupId: "Machine Learning", select: true },
	noOfMessages: { label: "No. Of Messages (Million)", value: 130, maxLength: 9, groupId: "Big Data" },
	noBC: { label: "No. Of BC", value: 500, maxLength: 5, groupId: "Precision Marketer" },
	noRealTimeBC: { label: "No. Of RealTime BC", value: 300, maxLength: 4, groupId: "Realtime Campaigns" },
	noOfRealTimeKPI: { label: "No Of RealTime KPI", value: 20, maxLength: 4, groupId: "Realtime Campaigns" },
	tgCgRequiredYN: { label: "tgCg Enabled", value: false, groupId: "Precision Marketer", select: true },
	concurrentUIHits: { label: "Concurrent UI Hits", value: 100, maxLength: 4, groupId: "Precision Marketer" },
	conAPIHits: { label: "Concurrent API Hits", value: 1000, maxLength: 5, groupId: "Intent Management" },
	imSecOffers: { label: "IM SegOffers (per Day)", value: 400, maxLength: 4, groupId: "Intent Management" },
	imRealTimeTriggerYN: { label: "IM RealTime Trigger Enabled", value: false, groupId: "Realtime Campaigns", select: true },
	wbExpectedModels: { label: "wbExpected Models (per Day)", value: 1, maxLength: 2, groupId: "Machine Learning" },
	wbIajJobExpected: { label: "wbIajJob Expected (per Day)", value: 20, maxLength: 3, groupId: "Machine Learning" },
	replicationFactor: { label: "ReplicationFactor", value: 3, maxLength: 1, optional: true, groupId: "Big Data" },
	logRetensionPeriod: { label: "Log Retension Period (Days)", value: 7, optional: true, maxLength: 2, groupId: "Big Data" },
	activeHours: { label: "ActiveHours (Business Hour)", value: 12, optional: true, maxLength: 2, groupId: "Precision Marketer" },
	controlGroup: { label: "Control Group (% of Base)", value: .1, optional: true, maxLength: 3, groupId: "Precision Marketer" },
	serverIPs: { label: "Server IPs", value: 1, groupId: "Installation", text: true },
	domainName: { label: "Domain Name", value: 1, groupId: "Installation", text: true }
	}

